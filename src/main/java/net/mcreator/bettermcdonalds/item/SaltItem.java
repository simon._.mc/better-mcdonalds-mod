
package net.mcreator.bettermcdonalds.item;

import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.mcreator.bettermcdonalds.init.BettermcdonaldsModTabs;

public class SaltItem extends Item {
	public SaltItem() {
		super(new Item.Properties().tab(BettermcdonaldsModTabs.TAB_INGREDIENTS_TAB).stacksTo(64).rarity(Rarity.COMMON));
	}

	@Override
	public UseAnim getUseAnimation(ItemStack itemstack) {
		return UseAnim.EAT;
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
