
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.bettermcdonalds.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.mcreator.bettermcdonalds.block.entity.LettucePlant2BlockEntity;
import net.mcreator.bettermcdonalds.block.entity.LettucePlant1BlockEntity;
import net.mcreator.bettermcdonalds.block.entity.LettucePlant0BlockEntity;
import net.mcreator.bettermcdonalds.BettermcdonaldsMod;

public class BettermcdonaldsModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES,
			BettermcdonaldsMod.MODID);
	public static final RegistryObject<BlockEntityType<?>> LETTUCE_PLANT_0 = register("lettuce_plant_0", BettermcdonaldsModBlocks.LETTUCE_PLANT_0,
			LettucePlant0BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> LETTUCE_PLANT_1 = register("lettuce_plant_1", BettermcdonaldsModBlocks.LETTUCE_PLANT_1,
			LettucePlant1BlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> LETTUCE_PLANT_2 = register("lettuce_plant_2", BettermcdonaldsModBlocks.LETTUCE_PLANT_2,
			LettucePlant2BlockEntity::new);

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block,
			BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
