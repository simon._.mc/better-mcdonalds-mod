
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.bettermcdonalds.init;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class BettermcdonaldsModTabs {
	public static CreativeModeTab TAB_FOOD_AND_DRINKS_TAB;
	public static CreativeModeTab TAB_INGREDIENTS_TAB;

	public static void load() {
		TAB_FOOD_AND_DRINKS_TAB = new CreativeModeTab("tabfood_and_drinks_tab") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(BettermcdonaldsModItems.HAPPY_MEAL.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_INGREDIENTS_TAB = new CreativeModeTab("tabingredients_tab") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(BettermcdonaldsModItems.TOMATO.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
