
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.bettermcdonalds.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;

import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Holder;

import net.mcreator.bettermcdonalds.world.features.ores.TomatoPlantFeature;
import net.mcreator.bettermcdonalds.world.features.ores.StrawberryPlantFeature;
import net.mcreator.bettermcdonalds.world.features.ores.SpiceLeafPlantFeature;
import net.mcreator.bettermcdonalds.world.features.ores.SaltOreFeature;
import net.mcreator.bettermcdonalds.world.features.OrangeTreeFeature;
import net.mcreator.bettermcdonalds.BettermcdonaldsMod;

import java.util.function.Supplier;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber
public class BettermcdonaldsModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, BettermcdonaldsMod.MODID);
	private static final List<FeatureRegistration> FEATURE_REGISTRATIONS = new ArrayList<>();
	public static final RegistryObject<Feature<?>> SALT_ORE = register("salt_ore", SaltOreFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, SaltOreFeature.GENERATE_BIOMES, SaltOreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> ORANGE_TREE = register("orange_tree", OrangeTreeFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.SURFACE_STRUCTURES, OrangeTreeFeature.GENERATE_BIOMES, OrangeTreeFeature::placedFeature));
	public static final RegistryObject<Feature<?>> STRAWBERRY_PLANT = register("strawberry_plant", StrawberryPlantFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, StrawberryPlantFeature.GENERATE_BIOMES,
					StrawberryPlantFeature::placedFeature));
	public static final RegistryObject<Feature<?>> TOMATO_PLANT = register("tomato_plant", TomatoPlantFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.UNDERGROUND_ORES, TomatoPlantFeature.GENERATE_BIOMES, TomatoPlantFeature::placedFeature));
	public static final RegistryObject<Feature<?>> SPICE_LEAF_PLANT = register("spice_leaf_plant", SpiceLeafPlantFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, SpiceLeafPlantFeature.GENERATE_BIOMES,
					SpiceLeafPlantFeature::placedFeature));

	private static RegistryObject<Feature<?>> register(String registryname, Supplier<Feature<?>> feature, FeatureRegistration featureRegistration) {
		FEATURE_REGISTRATIONS.add(featureRegistration);
		return REGISTRY.register(registryname, feature);
	}

	@SubscribeEvent
	public static void addFeaturesToBiomes(BiomeLoadingEvent event) {
		for (FeatureRegistration registration : FEATURE_REGISTRATIONS) {
			if (registration.biomes() == null || registration.biomes().contains(event.getName()))
				event.getGeneration().getFeatures(registration.stage()).add(registration.placedFeature().get());
		}
	}

	private static record FeatureRegistration(GenerationStep.Decoration stage, Set<ResourceLocation> biomes,
			Supplier<Holder<PlacedFeature>> placedFeature) {
	}
}
