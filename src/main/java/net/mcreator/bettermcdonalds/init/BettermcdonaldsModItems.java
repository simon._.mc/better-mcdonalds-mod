
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.bettermcdonalds.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.bettermcdonalds.item.TomatoItem;
import net.mcreator.bettermcdonalds.item.StrawberryMilkshakeItem;
import net.mcreator.bettermcdonalds.item.StrawberryItem;
import net.mcreator.bettermcdonalds.item.SpiceLeafItem;
import net.mcreator.bettermcdonalds.item.SaltItem;
import net.mcreator.bettermcdonalds.item.OrangeItem;
import net.mcreator.bettermcdonalds.item.MixingBowlItem;
import net.mcreator.bettermcdonalds.item.McSundaeItem;
import net.mcreator.bettermcdonalds.item.McSundaeChocolateItem;
import net.mcreator.bettermcdonalds.item.McChickenClassicItem;
import net.mcreator.bettermcdonalds.item.MayoItem;
import net.mcreator.bettermcdonalds.item.LettuceSeedsItem;
import net.mcreator.bettermcdonalds.item.LettuceItem;
import net.mcreator.bettermcdonalds.item.LemonItem;
import net.mcreator.bettermcdonalds.item.JuicerItem;
import net.mcreator.bettermcdonalds.item.HappyMealItem;
import net.mcreator.bettermcdonalds.item.HamburgerRoyalTSItem;
import net.mcreator.bettermcdonalds.item.HamburgerItem;
import net.mcreator.bettermcdonalds.item.GrinderItem;
import net.mcreator.bettermcdonalds.item.FriesItem;
import net.mcreator.bettermcdonalds.item.FantaItem;
import net.mcreator.bettermcdonalds.item.ColaItem;
import net.mcreator.bettermcdonalds.item.CocoaPowderItem;
import net.mcreator.bettermcdonalds.item.ChocolateMilkshakeItem;
import net.mcreator.bettermcdonalds.item.ChickenMcNuggetsItem;
import net.mcreator.bettermcdonalds.item.CheeseItem;
import net.mcreator.bettermcdonalds.item.CheeseBurgerItem;
import net.mcreator.bettermcdonalds.item.BigTastyBaconItem;
import net.mcreator.bettermcdonalds.item.BicMacItem;
import net.mcreator.bettermcdonalds.item.BeefPattyRawItem;
import net.mcreator.bettermcdonalds.item.BeefPattyItem;
import net.mcreator.bettermcdonalds.item.BakewareItem;
import net.mcreator.bettermcdonalds.BettermcdonaldsMod;

public class BettermcdonaldsModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, BettermcdonaldsMod.MODID);
	public static final RegistryObject<Item> JUICER = REGISTRY.register("juicer", () -> new JuicerItem());
	public static final RegistryObject<Item> MAYO = REGISTRY.register("mayo", () -> new MayoItem());
	public static final RegistryObject<Item> LETTUCE = REGISTRY.register("lettuce", () -> new LettuceItem());
	public static final RegistryObject<Item> SALT = REGISTRY.register("salt", () -> new SaltItem());
	public static final RegistryObject<Item> CHEESE = REGISTRY.register("cheese", () -> new CheeseItem());
	public static final RegistryObject<Item> TOMATO = REGISTRY.register("tomato", () -> new TomatoItem());
	public static final RegistryObject<Item> BAKEWARE = REGISTRY.register("bakeware", () -> new BakewareItem());
	public static final RegistryObject<Item> SPICE_LEAF = REGISTRY.register("spice_leaf", () -> new SpiceLeafItem());
	public static final RegistryObject<Item> STRAWBERRY = REGISTRY.register("strawberry", () -> new StrawberryItem());
	public static final RegistryObject<Item> COCOA_POWDER = REGISTRY.register("cocoa_powder", () -> new CocoaPowderItem());
	public static final RegistryObject<Item> GRINDER = REGISTRY.register("grinder", () -> new GrinderItem());
	public static final RegistryObject<Item> MIXING_BOWL = REGISTRY.register("mixing_bowl", () -> new MixingBowlItem());
	public static final RegistryObject<Item> SALT_ORE = block(BettermcdonaldsModBlocks.SALT_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> ORANGE = REGISTRY.register("orange", () -> new OrangeItem());
	public static final RegistryObject<Item> ORANGE_LEAVES = block(BettermcdonaldsModBlocks.ORANGE_LEAVES, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> LEMON = REGISTRY.register("lemon", () -> new LemonItem());
	public static final RegistryObject<Item> LETTUCE_PLANT_0 = block(BettermcdonaldsModBlocks.LETTUCE_PLANT_0, null);
	public static final RegistryObject<Item> LETTUCE_PLANT_1 = block(BettermcdonaldsModBlocks.LETTUCE_PLANT_1, null);
	public static final RegistryObject<Item> LETTUCE_PLANT_2 = block(BettermcdonaldsModBlocks.LETTUCE_PLANT_2, null);
	public static final RegistryObject<Item> LETTUCE_SEEDS = REGISTRY.register("lettuce_seeds", () -> new LettuceSeedsItem());
	public static final RegistryObject<Item> STRAWBERRY_PLANT = block(BettermcdonaldsModBlocks.STRAWBERRY_PLANT, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> TOMATO_PLANT = block(BettermcdonaldsModBlocks.TOMATO_PLANT, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> SPICE_LEAF_PLANT = block(BettermcdonaldsModBlocks.SPICE_LEAF_PLANT, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> MC_CHICKEN_CLASSIC = REGISTRY.register("mc_chicken_classic", () -> new McChickenClassicItem());
	public static final RegistryObject<Item> CHEESE_BURGER = REGISTRY.register("cheese_burger", () -> new CheeseBurgerItem());
	public static final RegistryObject<Item> HAMBURGER = REGISTRY.register("hamburger", () -> new HamburgerItem());
	public static final RegistryObject<Item> HAMBURGER_ROYAL_TS = REGISTRY.register("hamburger_royal_ts", () -> new HamburgerRoyalTSItem());
	public static final RegistryObject<Item> FRIES = REGISTRY.register("fries", () -> new FriesItem());
	public static final RegistryObject<Item> BIG_TASTY_BACON = REGISTRY.register("big_tasty_bacon", () -> new BigTastyBaconItem());
	public static final RegistryObject<Item> COLA = REGISTRY.register("cola", () -> new ColaItem());
	public static final RegistryObject<Item> STRAWBERRY_MILKSHAKE = REGISTRY.register("strawberry_milkshake", () -> new StrawberryMilkshakeItem());
	public static final RegistryObject<Item> CHOCOLATE_MILKSHAKE = REGISTRY.register("chocolate_milkshake", () -> new ChocolateMilkshakeItem());
	public static final RegistryObject<Item> MC_SUNDAE = REGISTRY.register("mc_sundae", () -> new McSundaeItem());
	public static final RegistryObject<Item> MC_SUNDAE_CHOCOLATE = REGISTRY.register("mc_sundae_chocolate", () -> new McSundaeChocolateItem());
	public static final RegistryObject<Item> CHICKEN_MC_NUGGETS = REGISTRY.register("chicken_mc_nuggets", () -> new ChickenMcNuggetsItem());
	public static final RegistryObject<Item> HAPPY_MEAL = REGISTRY.register("happy_meal", () -> new HappyMealItem());
	public static final RegistryObject<Item> BIC_MAC = REGISTRY.register("bic_mac", () -> new BicMacItem());
	public static final RegistryObject<Item> FANTA = REGISTRY.register("fanta", () -> new FantaItem());
	public static final RegistryObject<Item> BEEF_PATTY = REGISTRY.register("beef_patty", () -> new BeefPattyItem());
	public static final RegistryObject<Item> BEEF_PATTY_RAW = REGISTRY.register("beef_patty_raw", () -> new BeefPattyRawItem());

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
