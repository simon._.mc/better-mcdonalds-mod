
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.bettermcdonalds.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import net.mcreator.bettermcdonalds.block.TomatoPlantBlock;
import net.mcreator.bettermcdonalds.block.StrawberryPlantBlock;
import net.mcreator.bettermcdonalds.block.SpiceLeafPlantBlock;
import net.mcreator.bettermcdonalds.block.SaltOreBlock;
import net.mcreator.bettermcdonalds.block.OrangeLeavesBlock;
import net.mcreator.bettermcdonalds.block.LettucePlant2Block;
import net.mcreator.bettermcdonalds.block.LettucePlant1Block;
import net.mcreator.bettermcdonalds.block.LettucePlant0Block;
import net.mcreator.bettermcdonalds.BettermcdonaldsMod;

public class BettermcdonaldsModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, BettermcdonaldsMod.MODID);
	public static final RegistryObject<Block> SALT_ORE = REGISTRY.register("salt_ore", () -> new SaltOreBlock());
	public static final RegistryObject<Block> ORANGE_LEAVES = REGISTRY.register("orange_leaves", () -> new OrangeLeavesBlock());
	public static final RegistryObject<Block> LETTUCE_PLANT_0 = REGISTRY.register("lettuce_plant_0", () -> new LettucePlant0Block());
	public static final RegistryObject<Block> LETTUCE_PLANT_1 = REGISTRY.register("lettuce_plant_1", () -> new LettucePlant1Block());
	public static final RegistryObject<Block> LETTUCE_PLANT_2 = REGISTRY.register("lettuce_plant_2", () -> new LettucePlant2Block());
	public static final RegistryObject<Block> STRAWBERRY_PLANT = REGISTRY.register("strawberry_plant", () -> new StrawberryPlantBlock());
	public static final RegistryObject<Block> TOMATO_PLANT = REGISTRY.register("tomato_plant", () -> new TomatoPlantBlock());
	public static final RegistryObject<Block> SPICE_LEAF_PLANT = REGISTRY.register("spice_leaf_plant", () -> new SpiceLeafPlantBlock());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			LettucePlant0Block.registerRenderLayer();
			LettucePlant1Block.registerRenderLayer();
			LettucePlant2Block.registerRenderLayer();
			StrawberryPlantBlock.registerRenderLayer();
			TomatoPlantBlock.registerRenderLayer();
			SpiceLeafPlantBlock.registerRenderLayer();
		}

		@SubscribeEvent
		public static void blockColorLoad(ColorHandlerEvent.Block event) {
			OrangeLeavesBlock.blockColorLoad(event);
		}

		@SubscribeEvent
		public static void itemColorLoad(ColorHandlerEvent.Item event) {
			OrangeLeavesBlock.itemColorLoad(event);
		}
	}
}
